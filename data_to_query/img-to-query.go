package data_to_query

import (
	"fmt"
	"strconv"
	"strings"

	"fdps/fdps-parkings/db"
	"fdps/fdps-parkings/userhub"
	"fdps/utils/logger"
)

func UpdateImageToQuery(img userhub.ImageSettings) []db.QueryData {
	var retValue []db.QueryData

	var queryStr strings.Builder
	queryStr.WriteString("DECLARE ")
	queryStr.WriteString("img PARKING_PKG.IMAGE_INFO; ")
	queryStr.WriteString("BEGIN ")
	queryStr.WriteString(fmt.Sprintf("img.M_IMAGE_FILE_PATH := '%s'; ", img.ImageFilePath))
	//queryStr.WriteString(fmt.Sprintf("img.M_IMAGE_STRING := utl_raw.cast_to_raw('%s'); ", img.ImageBase64String))
	queryStr.WriteString(fmt.Sprintf("img.M_X0_LAT_GR := UTL_NUM.numFromStr('%s'); ", strconv.FormatFloat(img.X0, 'f', 12, 64)))
	queryStr.WriteString(fmt.Sprintf("img.M_Y0_LON_GR := UTL_NUM.numFromStr('%s'); ", strconv.FormatFloat(img.Y0, 'f', 12, 64)))
	queryStr.WriteString(fmt.Sprintf("img.M_X_PIXEL_DEG := UTL_NUM.numFromStr('%s'); ", strconv.FormatFloat(img.XScale, 'f', 12, 64)))
	queryStr.WriteString(fmt.Sprintf("img.M_Y_PIXEL_DEG := UTL_NUM.numFromStr('%s'); ", strconv.FormatFloat(img.YScale, 'f', 12, 64)))

	queryStr.WriteString("PARKING_PKG.UPDATE_IMAGE_PARAMS(img); COMMIT; END;")

	logger.PrintfWarn("%v", queryStr.String())
	retValue = append(retValue, db.QueryData{QueryText: queryStr.String()})

	return retValue
}

func SelectImgToQuery() []db.QueryData {
	return []db.QueryData{db.QueryData{
		QueryText: fmt.Sprintf(`SELECT 
		NVL(to_char(IMAGE_FILE_PATH), ' '),
		NVL(to_char(IMAGE_STRING), ' '),
		NVL(X0_LAT_GR, 0),
		NVL(Y0_LON_GR, 0),
		NVL(X_PIXEL_DEG, 0),
		NVL(Y_PIXEL_DEG, 0)
		FROM IMAGE`),
		SelectData: true,
		Func:       db.SelectImageFunc,
	}}
}
