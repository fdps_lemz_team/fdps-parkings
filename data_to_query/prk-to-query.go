package data_to_query

import (
	"fmt"
	"strings"

	"fdps/fdps-parkings/db"
	"fdps/fdps-parkings/userhub"
	"fdps/utils/logger"
)

// ParkingsFromAniToQuery - после запроса АНИ записываем стоянки полностью или обновляем X,Y, Список ВС
func ParkingsFromAniToQuery(prkList []userhub.ParkingInfo) []db.QueryData {
	var retValue []db.QueryData

	for _, val := range prkList {
		var queryStr strings.Builder
		queryStr.WriteString("DECLARE ")
		queryStr.WriteString("prk PARKING_PKG.PARKING_INFO; ")
		queryStr.WriteString("BEGIN ")
		queryStr.WriteString(fmt.Sprintf("prk.M_AIRPORT_CODE :='%s'; ", val.Airport))
		queryStr.WriteString(fmt.Sprintf("prk.M_PARKING_NAME := '%s'; ", val.Name))
		queryStr.WriteString(fmt.Sprintf("prk.M_X_POS := %d; ", val.XPos))
		queryStr.WriteString(fmt.Sprintf("prk.M_Y_POS := %d; ", val.YPos))
		var craftTypesStr string
		for _, val := range val.VsTypes {
			craftTypesStr += " " + val
		}
		queryStr.WriteString(fmt.Sprintf("prk.M_ARCFT_TYPES :='%s'; ", craftTypesStr))
		queryStr.WriteString(fmt.Sprintf("prk.M_CUR_STATE :='%s'; ", val.State))

		queryStr.WriteString("PARKING_PKG.INIT_PARKING_ANI(prk); COMMIT; END;")

		retValue = append(retValue, db.QueryData{QueryText: queryStr.String()})
	}
	return retValue
}

// ParkingsFromWebToQuery - после правок в web записываем поправки по X, Y и название объединенной стоянки
func ParkingsFromWebToQuery(origList []userhub.ParkingInfo, fromWebList []userhub.ParkingInfo) []db.QueryData {
	var retValue []db.QueryData

	for _, val := range fromWebList {
		for _, origVal := range origList {
			if origVal.Name == val.Name {
				if origVal.UnitedName != val.UnitedName || origVal.XAmmend != val.XAmmend || origVal.YAmmend != val.YAmmend {
					var queryStr strings.Builder
					queryStr.WriteString("DECLARE ")
					queryStr.WriteString("prk PARKING_PKG.PARKING_INFO; ")
					queryStr.WriteString("BEGIN ")
					queryStr.WriteString(fmt.Sprintf("prk.M_AIRPORT_CODE :='%s'; ", val.Airport))
					queryStr.WriteString(fmt.Sprintf("prk.M_PARKING_NAME := '%s'; ", val.Name))
					queryStr.WriteString(fmt.Sprintf("prk.M_UNITED_PARKING_NAME := '%s'; ", val.UnitedName))
					queryStr.WriteString(fmt.Sprintf("prk.M_X_POS_AMMEND := %d; ", val.XAmmend))
					queryStr.WriteString(fmt.Sprintf("prk.M_Y_POS_AMMEND := %d; ", val.YAmmend))

					queryStr.WriteString("PARKING_PKG.INIT_PARKING_WEB(prk); COMMIT; END;")

					retValue = append(retValue, db.QueryData{QueryText: queryStr.String()})
				}
			}
		}
	}
	return retValue
}

func UpdateParkingToQuery(prk userhub.ParkingInfo) []db.QueryData {
	var retValue []db.QueryData

	var queryStr strings.Builder
	queryStr.WriteString("DECLARE ")
	queryStr.WriteString("prk PARKING_PKG.PARKING_INFO; ")
	queryStr.WriteString("BEGIN ")
	queryStr.WriteString(fmt.Sprintf("prk.M_AIRPORT_CODE :='%s'; ", prk.Airport))
	queryStr.WriteString(fmt.Sprintf("prk.M_PARKING_NAME := '%s'; ", prk.Name))
	queryStr.WriteString(fmt.Sprintf("prk.M_CUR_STATE :='%s'; ", prk.State))
	queryStr.WriteString(fmt.Sprintf("prk.M_FLIGHT_NUM :='%s'; ", prk.FlightNumber))
	queryStr.WriteString(fmt.Sprintf("prk.M_SIDE_NUM :='%s'; ", prk.SideNumber))
	queryStr.WriteString(fmt.Sprintf("prk.M_INFO :='%s'; ", prk.Info))
	queryStr.WriteString(fmt.Sprintf("prk.M_SECTOR_CODE :='%s'; ", prk.AtcCode))

	queryStr.WriteString("PARKING_PKG.UPDATE_PARKING_STATE(prk); COMMIT; END;")

	logger.PrintfWarn("%v", queryStr.String())
	retValue = append(retValue, db.QueryData{QueryText: queryStr.String()})

	return retValue
}

func SelectParkingToQuery(ariportCode string) []db.QueryData {
	return []db.QueryData{db.QueryData{
		QueryText: fmt.Sprintf(`SELECT AIRPORT_ICAO_CODE, PARKING_NAME, NVL(UNITED_PARKING_NAME, ''), 
			(X_POS + X_POS_AMMEND), (Y_POS + Y_POS_AMMEND), X_POS_AMMEND, Y_POS_AMMEND, ARCFT_TYPES, NVL(CUR_STATE, 'Свободна'), 
			NVL(FLIGHT_NUM, ''), NVL(SIDE_NUM, ''), NVL(INFO, ''), NVL(SECTOR_CODE, '') 
			FROM PARKINGS WHERE AIRPORT_ICAO_CODE = '%s'`, ariportCode),
		SelectData: true,
		Func:       db.SelectParkingsFunc,
	}}
}
