package db

import (
	"database/sql"
	"errors"
	"strings"
	"sync"
	"time"

	"fdps/fdps-parkings/userhub"
	"fdps/utils/logger"

	"github.com/phf/go-queue/queue"
	//_ "gopkg.in/goracle.v2"
	_ "github.com/godror/godror"
)

const (
	dbStateKey       = "БД. Состояние подключения:"
	dbLastConnKey    = "БД. Последнее подключение:"
	dbLastDisconnKey = "БД. Последнее отключение:"
	dbLastErrKey     = "БД. Последняя ошибка:"

	dbStateOkValue    = "Подключено."    // значение параметра для подключенного состояния
	dbStateErrorValue = "Не подключено." // значение параметра для не подключенного состояния

	timeFormat = "2006-01-02 15:04:05"
)

// Controller контроллер БД FDPS
type Controller struct {
	sync.Mutex
	ConnStringChan chan string // канал приема новой строки подключения
	connString     string
	QueryChan      chan []QueryData // канал для приема запросов к БД
	closeChan      chan struct{}    // канал для передачи сигнала закрытия подключения к БД.
	ParkingChan    chan []userhub.ParkingInfo
	ImageSettsChan chan userhub.ImageSettings

	dataQueue queue.Queue // очередь сообщений для записи в БД

	db             *sql.DB    // объект БД
	dbSuccess      bool       // успешность подключения к БД
	execResultChan chan error // канал для передачи результатов выполнения
	canExecute     bool       // возможность выполнять запросы к БД
	//lastQueryText  string     // текст последнего запроса (при возникновении ошибки при упешном подключении выполнить еще раз)

	pingDbTicker    *time.Ticker // тикер пинга БД
	connectDbTicker *time.Ticker // тикер подключения к БД

	lastDbOpenErr string        // текст последней ошибки подключения к БД (чтоб в лог не писать одно и то же)
	doneChan      chan struct{} // канал для прекращения работы

	lastDbError     error  // последняя возникшая ошибка
	lastConnTime    string // время последнего успешного подключения
	lastDisconnTime string // время отключения
}

// NewController конструктор
func NewController(done chan struct{}) *Controller {
	return &Controller{
		ConnStringChan:  make(chan string, 1),
		QueryChan:       make(chan []QueryData, 1024),
		ParkingChan:     make(chan []userhub.ParkingInfo, 1),
		ImageSettsChan:  make(chan userhub.ImageSettings, 1),
		execResultChan:  make(chan error, 1),
		pingDbTicker:    time.NewTicker(3 * time.Second),
		connectDbTicker: time.NewTicker(10 * time.Second),
		doneChan:        done,
		lastDbError:     errors.New(""),
		lastConnTime:    "-",
		lastDisconnTime: "-",
	}
}

func (c *Controller) Work() {

	for {
		select {
		case newConnString := <-c.ConnStringChan:
			//web.SetDbQueueInfo(fmt.Sprintf("%d / %d", c.logQueue.Len(), logContainerSize))
			//web.SetDbSettings(fmt.Sprintf("%+v", newSettings))

			if c.connString != newConnString {
				c.connString = newConnString

				if c.dbSuccess {
					c.disconnectFromDb()
				}

				curErr := c.connectToDb()
				if curErr != nil {
					//c.StateChan <- chief_logger.CreateLoggerStateMsg(curErr, c.loggerVersion)
					c.checkQueryQueue()
				}

			}

		// получены новые запросы к БД
		case curQueries := <-c.QueryChan:
			c.Lock()
			for _, val := range curQueries {
				c.dataQueue.PushBack(val)
			}
			c.Unlock()
			c.checkQueryQueue()

		// пришел результат выполнения запроса из горутины
		case execErr := <-c.execResultChan:
			if execErr != nil {
				logger.PrintfErr("Error executing query ", execErr)
				if strings.Contains(execErr.Error(), "database is closed") ||
					strings.Contains(execErr.Error(), "server is not accepting clients") {
					c.disconnectFromDb()
				}
			}
			c.canExecute = true
			c.checkQueryQueue()

		// сработал тикер пингера БД
		case <-c.pingDbTicker.C:
			if c.dbSuccess {
				_ = c.heartbeat()
			}

			// сработал тикер подключения к БД
		case <-c.connectDbTicker.C:
			if !c.dbSuccess { //&& rlc.currentSettings.NeedWork {
				_ = c.connectToDb()
			}
		}
	}
}

func (c *Controller) connectToDb() error {
	c.dbSuccess = false
	curTime := time.Now().Format(timeFormat)

	var errOpen error
	if c.db, errOpen = sql.Open("godror", c.connString); errOpen != nil {

		if c.lastDbOpenErr != errOpen.Error() {
			logger.PrintfErr("Ошибка подключения к БД. Ошибка: %s", errOpen)
			logger.SetDebugParam(dbStateKey, dbStateErrorValue+"   "+curTime, logger.StateErrorColor)
			c.lastDbOpenErr = errOpen.Error()
		}
		return errOpen
	}

	if pingErr := c.db.Ping(); pingErr != nil {
		if pingErr.Error() != c.lastDbError.Error() {
			c.lastDbError = pingErr
			logger.PrintfErr("Ошибка выполнения PING БД. Ошибка: %s", pingErr)
			logger.SetDebugParam(dbLastErrKey, "Время: "+curTime+" Текст: "+pingErr.Error(), logger.StateErrorColor)
		}

		c.disconnectFromDb()
		return pingErr
	} else {
		c.lastDbError = errors.New("")
		logger.PrintfInfo("Успешное подключение к БД")
		logger.SetDebugParam(dbStateKey, dbStateOkValue+"   "+curTime, logger.StateOkColor)
		logger.SetDebugParam(dbLastConnKey, curTime, logger.StateDefaultColor)
	}
	c.dbSuccess = true
	c.canExecute = true
	c.checkQueryQueue()
	return nil
}

func (c *Controller) heartbeat() error {
	var heartbeatInt int
	hbtQueryStr := "SELECT 7 FROM dual"
	errHbt := c.db.QueryRow(hbtQueryStr).Scan(&heartbeatInt)
	if errHbt != nil {
		logger.PrintfErr("ошибка выполнения запроса HEARTBEAT к БД. Запрос: %s. Ошибка: %s ", hbtQueryStr, errHbt)
		c.disconnectFromDb()
	}
	return errHbt
}

func (c *Controller) disconnectFromDb() {
	c.canExecute = false
	c.dbSuccess = false
	c.db.Close()

	logger.SetDebugParam(dbStateKey, dbStateErrorValue+"   "+time.Now().Format(timeFormat), logger.StateErrorColor)
}

func (c *Controller) checkQueryQueue() {
	if c.canExecute {
		if c.dataQueue.Len() > 0 {
			c.canExecute = false
			curQuery := c.dataQueue.PopFront().(QueryData)
			var errExec error

			if curQuery.SelectData {
				var rows *sql.Rows
				if rows, errExec = c.db.Query(curQuery.QueryText); errExec == nil {
					if curQuery.Func != nil {
						if prkListIface, errFunc := curQuery.Func(rows); errFunc == nil {
							if prkList, ok := prkListIface.([]userhub.ParkingInfo); ok {
								c.ParkingChan <- prkList
							}
							if imgSetts, ok := prkListIface.(userhub.ImageSettings); ok {
								c.ImageSettsChan <- imgSetts
							}
						} else {
							logger.PrintfErr("Ошибка обработки результатов запроса. Запрос: %s. Ошибка: %s", curQuery.QueryText, errFunc)
						}
					} else {
						rows.Close()
					}
				}
			} else {
				_, errExec = c.db.Exec(curQuery.QueryText)
			}

			c.execResultChan <- errExec
		}
	}
}

// SelectParkingsFunc
func SelectParkingsFunc(curRows *sql.Rows) (selectedData interface{}, retErr error) {
	var retValue []userhub.ParkingInfo

	defer curRows.Close()

	for curRows.Next() {
		var prk userhub.ParkingInfo
		var vsString string
		if retErr := curRows.Scan(&prk.Airport, &prk.Name, &prk.UnitedName, &prk.XPos, &prk.YPos, &prk.XAmmend, &prk.YAmmend,
			&vsString, &prk.State, &prk.FlightNumber, &prk.SideNumber, &prk.Info, &prk.AtcCode); retErr == nil {
			prk.VsTypes = append(prk.VsTypes, vsString)
			retValue = append(retValue, prk)
		} else {
			return retValue, retErr
		}
	}

	return retValue, retErr
}

// SelectImageFunc
func SelectImageFunc(curRows *sql.Rows) (selectedData interface{}, retErr error) {
	var retValue userhub.ImageSettings

	defer curRows.Close()

	for curRows.Next() {
		retErr = curRows.Scan(&retValue.ImageFilePath, &retValue.ImageBase64String, &retValue.X0, &retValue.Y0, &retValue.XScale, &retValue.YScale)
	}

	return retValue, retErr
}
