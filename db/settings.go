package db

import "strconv"

// Settings настройки подключения к БД
type Settings struct {
	Hostname    string `json:"DbHostname"`
	Port        int    `json:"DbPort"`
	ServiceName string `json:"ServiceName"`
	UserName    string `json:"DbUser"`
	Password    string `json:"DbPassword"`
}

// InitByDefault
func (s *Settings) InitByDefault() {
	s.Hostname = "localhost"
	s.Port = 1251
	s.ServiceName = "plan"
	s.UserName = "parkinguser"
	s.Password = "parkingpass"
}

// ConnString - строка подключения к БД в формате
// user/pass@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=tcp)(HOST=hostname)(PORT=port)))(CONNECT_DATA=(SERVICE_NAME=sn)))
func (s *Settings) ConnString() string {
	return s.UserName + "/" +
		s.Password + "@" +
		"(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=tcp)(HOST=" + s.Hostname +
		")(PORT=" + strconv.Itoa(s.Port) +
		")))(CONNECT_DATA=(SERVICE_NAME=" + s.ServiceName + ")))"

}
