package userhub

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"fdps/utils"
	"fdps/utils/logger"
	"fdps/utils/web_sock"

	"github.com/gorilla/websocket"
)

const (
	srvStateKey       = "WS. Состояние:"
	srvLastConnKey    = "WS. Последнее клиентское подключение:"
	srvLastDisconnKey = "WS. Последнее клиентское отключение:"
	srvLastErrKey     = "WS. Последняя ошибка:"
	srvClntListKey    = "WS. Список клиентов:"

	srvStateOkValue    = "Запущен."
	srvStateErrorValue = "Не запущен."

	timeFormat = "2006-01-02 15:04:05"
)

// Settings настройки WS сервера
type Settings struct {
	Port int `json:"Port"`
}

// Server
type Server struct {
	SettsChan         chan Settings
	setts             Settings
	killerChan        chan struct{} // канал, по которому передается сигнал о завершение работы канала
	doneChan          chan struct{}
	wsServer          *web_sock.WebSockServer
	stateValidDur     time.Duration // интервал, после которого, если нет сведений о канале, сичтаем его нерабочим
	ParkingChan       chan []ParkingInfo
	ParkingChangeChan chan ParkingInfo
	ImageSettsChan    chan ImageSettings
	imgSetts          ImageSettings

	Parkings []ParkingInfo
}

// NewServer конструктор
func NewServer(done chan struct{}) *Server {
	return &Server{
		SettsChan:         make(chan Settings, 10),
		killerChan:        make(chan struct{}),
		doneChan:          done,
		wsServer:          web_sock.NewWebSockServer(done),
		stateValidDur:     time.Second * 3,
		ParkingChan:       make(chan []ParkingInfo, 10),
		ParkingChangeChan: make(chan ParkingInfo, 1),
		ImageSettsChan:    make(chan ImageSettings, 1),
	}
}

// Work реализация работы
func (s *Server) Work() {
	go s.wsServer.Work("/" + utils.ParkingClientsPath)

	logger.SetDebugParam(srvStateKey, srvStateErrorValue+"   "+time.Now().Format(timeFormat), logger.StateErrorColor)
	logger.SetDebugParam(srvLastConnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastDisconnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastErrKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvClntListKey, "-", logger.StateDefaultColor)

	sendParkingFunc := func(sock *websocket.Conn) {
		if parkingsData, err := json.Marshal(CreateAnswerParkingsMsg(s.Parkings)); err == nil {
			s.wsServer.SendDataChan <- web_sock.WsPackage{Data: parkingsData, Sock: sock}
		}
	}

	sendImageFunc := func(sock *websocket.Conn) {
		logger.PrintfErr("ImgCntrl.imageBase64String %v", ImgCntrl.GetImageSetts().ImageBase64String)
		if ImgCntrl.lastError == nil {
			if parkingsData, err := json.Marshal(CreateAnswerImageMsg(ImgCntrl.GetImageSetts().ImageBase64String)); err == nil {
				s.wsServer.SendDataChan <- web_sock.WsPackage{Data: parkingsData, Sock: sock}
			}
		}
	}

	for {
		select {
		// получены новые настройки
		case newSetts := <-s.SettsChan:
			if newSetts != s.setts {
				s.setts = newSetts
				s.wsServer.SettingsChan <- web_sock.WebSockServerSettings{Port: s.setts.Port}
			}

		// получены сведения о стоянках
		case s.Parkings = <-s.ParkingChan:
			sendParkingFunc(nil)

		case s.imgSetts = <-s.ImageSettsChan:
			sendImageFunc(nil)

		// получен подключенный клиент от WS сервера
		case curClnt := <-s.wsServer.ClntConnChan:
			logger.PrintfInfo("Подключен клиент с адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastConnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)

		// получен отключенный клиент от WS сервера
		case curClnt := <-s.wsServer.ClntDisconnChan:
			logger.PrintfInfo("Отключен клиентс адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastDisconnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)

		// получен отклоненный клиент от WS сервера
		case curClnt := <-s.wsServer.ClntRejectChan:
			logger.PrintfInfo("Отклонен клиент с адресом: %s.", curClnt.RemoteAddr().String())

		// получена ошибка от WS сервера
		case wsErr := <-s.wsServer.ErrorChan:
			logger.PrintfErr("Возникла ошибка при работе WS сервера. Ошибка: %s.", wsErr.Error())
			logger.SetDebugParam(srvLastErrKey, wsErr.Error(), logger.StateErrorColor)

		// получены данные от WS сервера
		case curWsPkg := <-s.wsServer.ReceiveDataChan:
			logger.PrintfErr(string(curWsPkg.Data))
			var curHdr HeaderMsg
			var unmErr error
			if unmErr = json.Unmarshal(curWsPkg.Data, &curHdr); unmErr == nil {
				switch curHdr.Header {

				case RequestParkingsHdr:
					sendParkingFunc(curWsPkg.Sock)

				case RequestImageHdr:
					sendImageFunc(curWsPkg.Sock)

				case ClientHeartbeatHdr:
					var curHbtMsg ClientHeartbeatMsg
					if err := json.Unmarshal(curWsPkg.Data, &curHbtMsg); err == nil {
						//s.chStates[curHbtMsg.ID] = сhannelStateTime{State: curHbtMsg.State, Time: time.Now()}
					}

				case ClientParkingChangeHdr:
					var curMsg ClientParkingChangeMsg
					if err := json.Unmarshal(curWsPkg.Data, &curMsg); err == nil {
						logger.PrintfErr("Get parking change")
						s.ParkingChangeChan <- curMsg.Parking

						for ind, val := range s.Parkings {
							if val.Name == curMsg.Parking.Name {
								s.Parkings[ind] = curMsg.Parking
							}
						}

						// уведомление всех клиентов
						if parkingsData, err := json.Marshal(CreateServerParkingChangeMsg(&curMsg.Parking)); err == nil {
							s.wsServer.SendDataChan <- web_sock.WsPackage{Data: parkingsData}
						}
					}
				}

			} else {
				logger.PrintfErr("Ошибка разбора сообщения от приложения AFTN канала. Ошибка: %s.", unmErr)
			}

		// получено состояние работоспособности WS сервера для связи с AFTN каналами
		case connState := <-s.wsServer.StateChan:
			switch connState {
			case web_sock.ServerTryToStart:
				logger.PrintfInfo("Запускаем WS сервер для взаимодействия. Порт: %d Path: %s.", s.setts.Port, utils.ParkingClientsPath)
				logger.SetDebugParam(srvStateKey, srvStateOkValue+" Порт: "+strconv.Itoa(s.setts.Port)+" Path: "+utils.ParkingClientsPath, logger.StateOkColor)
			case web_sock.ServerError:
				logger.SetDebugParam(srvStateKey, srvStateErrorValue, logger.StateErrorColor)
				//case web_sock.ServerReadySend:
				//case web_sock.ServerNotReadySend:
			}

			// таймер отправки состояния каналов
			//case <-s.stateTkr.C:
			// var chStateSlice []chanstate.State
			// nowTime := time.Now()
			// for key, val := range s.chStates {
			// 	if val.Time.Add(s.stateValidDur).Before(nowTime) {
			// 		delete(s.chStates, key)
			// 	} else {
			// 		chStateSlice = append(chStateSlice, s.chStates[key].State)
			// 	}
			// }

			// s.ChannelStates <- chStateSlice
		}
	}
}
