package userhub

import (
	"strings"

	"fdps/fdps-parkings/ani"
)

const (
	ClosedState   = "Закрыта"
	FreeState     = "Свободна"
	OccupiedState = "Занята"
)

// ParkingInfo информация о стоянке
type ParkingInfo struct {
	Name         string   `json:"Name"`       // название стоянки
	Airport      string   `json:"Airport"`    // ICAO код аэродрома
	UnitedName   string   `json:"UnitedName"` // координата Y на сцене
	VsTypes      []string `json:"CraftTypes"` // типы принимаемых ВС
	State        string   `json:"State"`      // состояние стоянки
	FlightNumber string   `json:"Flight"`     // номер рейса
	SideNumber   string   `json:"Side"`       // бортовой номер
	Info         string   `json:"Info"`       // примечание
	XPos         int      `json:"X"`          // координата X на сцене
	YPos         int      `json:"Y"`          // координата Y на сцене
	XAmmend      int      `json:"XAmmend"`    // правка по координате X на сцене
	YAmmend      int      `json:"YAmmend"`    // правка по координате Y на сцене
	AtcCode      string   `json:"AtcCode"`    // код сектора, изменивщего состояние
}

// FromAniItem преобразование из формата АНИ
func FromAniItem(aniItem ani.Item, airportCode string) ParkingInfo {
	craftTypes := strings.Split(aniItem.Prop.AircraftTypes, "\\")

	var curParking = ParkingInfo{
		Name:    aniItem.Prop.Code,
		Airport: airportCode,
		VsTypes: craftTypes,
		State:   FreeState,
	}

	curParking.XPos, curParking.YPos = ImgCntrl.PosFromCoord(aniItem.Geom.Coordnates)

	return curParking
}

// var x0 float32 = 129.74710467
// var y0 float32 = 62.102611157

// var xScale float32 = 0.00006440476
// var yScale float32 = -0.000031736843

// // var xScale float32 = 0.00006640476
// // var yScale float32 = -0.000031736843

// func posFromCoord(latLon []float32) (xPos int, yPos int) {

// 	xPos = int((latLon[0] - x0) / xScale)
// 	yPos = int((latLon[1] - y0) / yScale)

// 	return xPos, yPos
// }
