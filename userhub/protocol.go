package userhub

const (
	// RequestParkingsHdr заголовок сообщения запроса стоянок
	RequestParkingsHdr = "RequestParkings"

	// AnswerParkingsHdr заголовок сообщения со сведениями о стоянках
	AnswerParkingsHdr = "AnswerParkings"

	// RequestImageHdr заголовок сообщения запроса картинки
	RequestImageHdr = "RequestImage"

	// AnswerImageHdr заголовок сообщения с картинкой
	AnswerImageHdr = "AnswerImage"

	// ServerImageChangeHdr заголовок сообщения об изменениии картинки
	ServerImageChangeHdr = "ServerImageChange"

	// ClientHeartbeatHdr заголовок сообщения о состоянии клиента
	ClientHeartbeatHdr = "ClientHeartbeat"

	// ClientParkingChangeHdr заголовок сообщения об изменениии состояния стоянки от клиента
	ClientParkingChangeHdr = "ClientParkingChange"

	// ServerParkingChangeHdr заголовок сообщения об изменениии состояния стоянки от сервера
	ServerParkingChangeHdr = "ServerParkingChange"
)

// HeaderMsg описание заголовка сообщений
type HeaderMsg struct {
	Header string `json:"MessageType"`
}

// RequestParkingsMsg сообщение запроса настроек канала
// клиент -> сервер
type RequestParkingsMsg struct {
	HeaderMsg
}

// CreateRequestParkingsMsg сформировать сообщение запроса стоянок
func CreateRequestParkingsMsg() RequestParkingsMsg {
	return RequestParkingsMsg{HeaderMsg: HeaderMsg{Header: RequestParkingsHdr}}
}

// AnswerParkingsMsg сообщения со сведениями о стоянках
// сервер -> клиент
type AnswerParkingsMsg struct {
	HeaderMsg
	Parkings []ParkingInfo `json:"Parkings"`
}

// CreateAnswerParkingsMsg сформировать сообщение со сведениями о стоянках
func CreateAnswerParkingsMsg(p []ParkingInfo) AnswerParkingsMsg {
	return AnswerParkingsMsg{HeaderMsg: HeaderMsg{Header: AnswerParkingsHdr}, Parkings: p}
}

// RequestImageMsg сообщение запроса картинки
// клиент -> сервер
type RequestImageMsg struct {
	HeaderMsg
}

// CreateRequestImageMsg сформировать сообщение запроса картинки
func CreateRequestImageMsg() RequestImageMsg {
	return RequestImageMsg{HeaderMsg: HeaderMsg{Header: RequestImageHdr}}
}

// AnswerImageMsg сообщения с картинкой
// сервер -> клиент
type AnswerImageMsg struct {
	HeaderMsg
	ImageBase64 string `json:"ImageBase64"`
}

// CreateAnswerImageMsg сформировать сообщение с картинкой
func CreateAnswerImageMsg(bs string) AnswerImageMsg {
	return AnswerImageMsg{HeaderMsg: HeaderMsg{Header: AnswerImageHdr}, ImageBase64: bs}
}

// ServerImageChangeMsg сообщение об изменении картинки
// сервер -> клиент
type ServerImageChangeMsg struct {
	HeaderMsg
	ImageBase64 string `json:"ImageBase64"`
}

// CreateServerImageChangeMsg сформировать сообщение об изменении картинки
func CreateServerImageChangeMsg(bs string) ServerImageChangeMsg {
	return ServerImageChangeMsg{HeaderMsg: HeaderMsg{Header: ServerImageChangeHdr}, ImageBase64: bs}
}

// ClientHeartbeatMsg сообщение о состоянии канала
// клиент -> сервер
type ClientHeartbeatMsg struct {
	HeaderMsg
	State string `json:"State"`
}

// CreateClientHeartbeatMsg сформировать сообщение о состоянии клиента
func CreateClientHeartbeatMsg() ClientHeartbeatMsg {
	return ClientHeartbeatMsg{HeaderMsg: HeaderMsg{Header: ClientHeartbeatHdr}, State: "OK"}
}

// ClientParkingChangeMsg сообщение об изменении состояния стоянки клиентом
// клиент -> tсервер
type ClientParkingChangeMsg struct {
	HeaderMsg
	Parking ParkingInfo `json:"Parking"`
}

// CreateClientParkingChangeMsg сформировать сообщение об изменении состояния стоянки клиентом
func CreateClientParkingChangeMsg(p *ParkingInfo) ClientParkingChangeMsg {
	return ClientParkingChangeMsg{HeaderMsg: HeaderMsg{Header: ClientParkingChangeHdr}, Parking: *p}
}

// ServerParkingChangeMsg сообщение об изменении состояния стоянки сервером
// сервер -> клиент
type ServerParkingChangeMsg struct {
	HeaderMsg
	Parking ParkingInfo `json:"Parking"`
}

// CreateServerParkingChangeMsg сформировать сообщение об изменении состояния стоянки сервером
func CreateServerParkingChangeMsg(p *ParkingInfo) ServerParkingChangeMsg {
	return ServerParkingChangeMsg{HeaderMsg: HeaderMsg{Header: ServerParkingChangeHdr}, Parking: *p}
}
