package userhub

import (
	"bufio"
	"encoding/base64"
	"io/ioutil"
	"os"
)

// параметры подложки
type ImageSettings struct {
	ImageFilePath     string  // путь к файлу картинки
	ImageBase64String string  // Картинка подложки в base64
	X0                float64 // Географическая широта левого верхнего угла картинки
	Y0                float64 // Географическая долгота левого верхнего угла картинки
	XScale            float64 // Кол-во градусов широты в пикселе
	YScale            float64 // Кол-во градусов доготы в пикселе
}

// ImageController контроллер, предоставляющий картинку для подложки
type ImageController struct {
	setts ImageSettings

	lastError error
}

var ImgCntrl ImageController

func (ic *ImageController) SetImageSetts(setts ImageSettings) error {
	ic.setts = setts
	var imgfile *os.File
	if imgfile, ic.lastError = os.Open(ic.setts.ImageFilePath); ic.lastError != nil {
		return ic.lastError
	} else {
		defer imgfile.Close()

		reader := bufio.NewReader(imgfile)
		var content []byte
		if content, ic.lastError = ioutil.ReadAll(reader); ic.lastError != nil {
			return ic.lastError
		} else {
			ic.setts.ImageBase64String = base64.StdEncoding.EncodeToString(content)
		}
	}

	return nil
}

func (ic *ImageController) GetImageSetts() ImageSettings {
	return ic.setts
}

func (ic *ImageController) PosFromCoord(latLon []float64) (xPos int, yPos int) {
	return int((latLon[0] - ic.setts.X0) / ic.setts.XScale),
		int((latLon[1] - ic.setts.Y0) / ic.setts.YScale)
}
