DROP USER parkinguser CASCADE;
DROP PROFILE parking CASCADE;
COMMIT;

--error: ORA-65096: invalid common user or role name in oracle
alter session set "_ORACLE_SCRIPT"=true;  

create profile parking limit
cpu_per_session default
cpu_per_call default
connect_time default
idle_time default
sessions_per_user default
logical_reads_per_session default
logical_reads_per_call default
private_sga default
composite_limit default
password_life_time unlimited
password_grace_time default
password_reuse_max default
password_reuse_time default
password_verify_function default
failed_login_attempts unlimited
password_lock_time default;
commit;

ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;
commit;
create user  parkinguser
profile parking
identified by parkingpass
account unlock 
default tablespace users 
temporary tablespace temp;  
commit;  
grant resource 			to parkinguser;
grant connect 			to parkinguser;
grant create session 	to parkinguser;
grant alter session 	to parkinguser;
grant create any table	to parkinguser;
grant create any view	to parkinguser;
grant create view		to parkinguser;
grant create synonym	to parkinguser;
commit;

--ORA-01950: ��� ���������� �� ������ 'USERS'
ALTER USER parkinguser quota unlimited on users;
