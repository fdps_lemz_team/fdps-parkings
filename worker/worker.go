package worker

import (
	"sync"

	"fdps/fdps-parkings/ani"
	"fdps/fdps-parkings/data_to_query"
	"fdps/fdps-parkings/db"
	"fdps/fdps-parkings/settings"
	"fdps/fdps-parkings/userhub"
	"fdps/fdps-parkings/web"
	"fdps/utils/logger"
)

var (
	akeyForSave string
	akeyForSend string
)

func Start(done chan struct{}, wg *sync.WaitGroup) {
	var setts settings.Settings
	if errRead := setts.ReadFromFile(); errRead != nil {
		logger.PrintfErr("Ошибка чтения настроек из файла: %v", errRead)
		setts.DefaultInit()
		logger.PrintfInfo("Настройки инициализирован значениями по умолчанию: %#v", setts)
		if errSave := setts.SaveToFile(); errSave != nil {
			logger.PrintfErr("Ошибка сохранения настроек в файл: %v", errSave)
		} else {
			logger.PrintfErr("Настройки сохранены в файл")
		}
	}

	aniClient := ani.NewClient()
	go aniClient.Work()
	aniClient.SettsChan <- setts.AniSetts
	web.SetAniSetts(setts.AniSetts)

	userServer := userhub.NewServer(done)
	go userServer.Work()
	userServer.SettsChan <- userhub.Settings{Port: setts.WsSetts.Port}
	web.SetWsSetts(userhub.Settings{Port: setts.WsSetts.Port})

	dbClient := db.NewController(done)
	go dbClient.Work()
	dbClient.ConnStringChan <- setts.DbSetts.ConnString()
	web.SetDbSetts(setts.DbSetts)
	// запрос подложки
	dbClient.QueryChan <- data_to_query.SelectImgToQuery()

	// запрос стоянок
	dbClient.QueryChan <- data_to_query.SelectParkingToQuery(setts.AniSetts.AirdromName)

	//imgCntrl := userhub.CreateImageController()
	// if errImg := userhub.ImgCntrl.SetImagePath(setts.PixmapPath); errImg != nil {
	// 	logger.PrintfErr("%v", errImg)
	// }

	go func() {
		for {
			select {
			case aniParkingItems := <-aniClient.AniItemsChan:

				var parkings []userhub.ParkingInfo
				for _, prk := range aniParkingItems {
					parkings = append(parkings, userhub.FromAniItem(prk, setts.AniSetts.AirdromName))
				}

				// обновление стоянок данными из АНИ
				dbClient.QueryChan <- data_to_query.ParkingsFromAniToQuery(parkings)
				// запрос стоянок
				dbClient.QueryChan <- data_to_query.SelectParkingToQuery(setts.AniSetts.AirdromName)

			case imgSetts := <-dbClient.ImageSettsChan:
				userhub.ImgCntrl.SetImageSetts(imgSetts)
				userServer.ImageSettsChan <- imgSetts
				web.SetImgSetts(imgSetts)

			case prkList := <-dbClient.ParkingChan:
				userServer.ParkingChan <- prkList
				toWebList := append([]userhub.ParkingInfo(nil), prkList...)
				web.SetParkings(toWebList)

			case setts.AniSetts = <-web.AniSettsChan:
				if errSave := setts.SaveToFile(); errSave != nil {
					logger.PrintfErr("Ошибка сохранения настроек: %v.")
				}
				aniClient.SettsChan <- setts.AniSetts

			case setts.DbSetts = <-web.DbSettsChan:
				if errSave := setts.SaveToFile(); errSave != nil {
					logger.PrintfErr("Ошибка сохранения настроек: %v.")
				}
				dbClient.ConnStringChan <- setts.DbSetts.ConnString()

			case setts.WsSetts = <-web.WsSettsChan:
				if errSave := setts.SaveToFile(); errSave != nil {
					logger.PrintfErr("Ошибка сохранения настроек: %v.")
				}
				userServer.SettsChan <- setts.WsSetts

			case imgSetts := <-web.ImgSettsChan:
				userhub.ImgCntrl.SetImageSetts(imgSetts)

				if queryList := data_to_query.UpdateImageToQuery(userhub.ImgCntrl.GetImageSetts()); len(queryList) > 0 {
					// обновление подложки данными из WEB
					dbClient.QueryChan <- queryList
					// запрос подложки
					dbClient.QueryChan <- data_to_query.SelectImgToQuery()
				}

			case prkFromWeb := <-web.PrkChan:
				if queryList := data_to_query.ParkingsFromWebToQuery(userServer.Parkings, prkFromWeb); len(queryList) > 0 {
					// обновление стоянок данными из WEB
					dbClient.QueryChan <- queryList
					// запрос стоянок
					dbClient.QueryChan <- data_to_query.SelectParkingToQuery(setts.AniSetts.AirdromName)
				}

			case prk := <-userServer.ParkingChangeChan:
				queryStrList := data_to_query.UpdateParkingToQuery(prk)
				dbClient.QueryChan <- queryStrList

			case <-done:
				wg.Done()
				return

			}
		}
	}()
}
