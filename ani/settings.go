package ani

import "fmt"

// Settings настройки для взаимодействия с web сервером АНИ
// URL АНИ имеет следующий формат: IP-адрес сервера:Порт/fdani/web/data/aprdgates/ardm_icaolat
type Settings struct {
	Host        string `json:"AniHost"`
	Port        int    `json:"AniPort"`
	AirdromName string `json:"AirdromName"`
}

// InitByDefault
func (s *Settings) InitByDefault() {
	s.Host = "192.168.1.43"
	s.Port = 8080
	s.AirdromName = "UEEE"
}

// GetUrl
func (s *Settings) GetUrl() string {
	return fmt.Sprintf("http://%s:%d/fdani/web/data/aprdgates/%s", s.Host, s.Port, s.AirdromName)
}
