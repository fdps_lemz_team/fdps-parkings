package ani

// ответ от сервера АНИ имеет вид:
// "gates": [
//     {
//       "type": "gate",
//       "properties": {
//         "parking_id": 7181,
//         "parkingcode": "1",
//         "acfttype": ""
//       },
//       "geometry": {
//         "type": "Point",
//         "coordinates": [
//           129.751711,
//           62.087314
//         ]
//       }
// 	}

// Properties
type Properties struct {
	Id            int    `json:"parking_id"`
	Code          string `json:"parkingcode"`
	AircraftTypes string `json:"acfttype"`
}

// Geometry
type Geometry struct {
	Type       string    `json:"type"`
	Coordnates []float64 `json:"coordinates"`
}

// Item
type Item struct {
	Type string     `json:"type"`
	Prop Properties `json:"properties"`
	Geom Geometry   `json:"geometry"`
}

// ParkingItemsMsg ответ на запрос стоянок
type ParkingItemsMsg struct {
	Items []Item `json:"gates"`
}
