package ani

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"fdps/utils/logger"
)

const (
	lastPostTimeKey        = "Время последнего GET:"
	lastSuccessPostTimeKey = "Время последнего успешного GET:"
	lastFailPostTimeKey    = "Время последнего неуспешного GET:"
	lastPostErrorKey       = "Последняя ошибка GET:"
	timeFormat             = "2006-01-02 15:04:05"
)

// клиент для подключения к конфигуратору
type Client struct {
	SettsChan      chan Settings // канал для приема настроек
	setts          Settings      // текущие настройки
	AniItemsChan   chan []Item   // канал для передачи элементов стоянок
	lastPostErr    error
	postResultChan chan json.RawMessage
}

// NewClient конструктор
func NewClient() *Client {
	return &Client{
		SettsChan:      make(chan Settings),
		AniItemsChan:   make(chan []Item, 10),
		lastPostErr:    errors.New(""),
		postResultChan: make(chan json.RawMessage, 10),
	}
}

// Work рабочий цикл
func (c *Client) Work() {
	logger.SetDebugParam(lastPostTimeKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(lastFailPostTimeKey, "-", logger.StateWarningColor)
	logger.SetDebugParam(lastSuccessPostTimeKey, "-", logger.StateOkColor)
	logger.SetDebugParam(lastPostErrorKey, "-", logger.StateWarningColor)

	for {
		select {
		// получены новые настройки
		case newSetts := <-c.SettsChan:
			if newSetts != c.setts {
				c.setts = newSetts
				c.postParkingRequest()
			}

		// результат выполнения POST запроса
		case postRes := <-c.postResultChan:
			var msg ParkingItemsMsg
			if unmErr := json.Unmarshal(postRes, &msg); unmErr != nil {
				logger.PrintfErr("Ошибка разбора (unmarshall) сообщения АНИ. Сообщение: %s. Ошибка: %s.", string(postRes), unmErr.Error())
			} else {
				// отправляем данные о стоянках
				c.AniItemsChan <- msg.Items
			}
		}
	}
}

func (c *Client) postParkingRequest() error {
	errFunc := func(err error) error {
		logger.PrintfWarn("%v", err)
		logger.SetDebugParam(lastFailPostTimeKey, time.Now().Format(timeFormat), logger.StateWarningColor)
		logger.SetDebugParam(lastPostErrorKey, err.Error(), logger.StateWarningColor)
		return err
	}

	resp, postErr := http.Get(c.setts.GetUrl())
	logger.SetDebugParam(lastPostTimeKey, time.Now().Format(timeFormat), logger.StateDefaultColor)
	if postErr == nil {
		defer resp.Body.Close()
		if strings.Contains(resp.Status, "200") {
			if body, readErr := ioutil.ReadAll(resp.Body); readErr == nil {
				if bytes.Contains(body, []byte("error")) {
					return errFunc(fmt.Errorf("Не валидный пакет: %s", string(body)))
				}
				if ind := strings.Index(string(body), "{"); ind >= 0 {
					c.postResultChan <- body[ind:]
				} else {
					return errFunc(fmt.Errorf("Не валидный пакет: %s", string(body)))
				}
			} else {
				return errFunc(readErr)
			}
		} else {
			return errFunc(fmt.Errorf("Статус ответа: %s", resp.Status))
		}
	} else {
		return errFunc(postErr)
	}

	logger.SetDebugParam(lastSuccessPostTimeKey, time.Now().Format(timeFormat), logger.StateOkColor)
	return nil
}
