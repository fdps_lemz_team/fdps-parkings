package web

import (
	"net/http"
	"strconv"

	"fdps/utils"
)

type ImgSaveHandler struct {
	handleURL string
	title     string
}

var ImgSaveHdl ImgSaveHandler

func (sch ImgSaveHandler) Path() string {
	return sch.handleURL
}

func (sch ImgSaveHandler) Caption() string {
	return sch.title
}

func (sch ImgSaveHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return imgSaveHandler
}

func InitImgSaveHandler(handleURL string, title string) {
	ImgSaveHdl.handleURL = "/" + handleURL
	ImgSaveHdl.title = title
}

func imgSaveHandler(w http.ResponseWriter, r *http.Request) {

	srv.imgPage.ImgSetts.ImageFilePath = r.FormValue("ImagePath")
	srv.imgPage.ImgSetts.X0, _ = strconv.ParseFloat(r.FormValue("X0"), 32)
	srv.imgPage.ImgSetts.Y0, _ = strconv.ParseFloat(r.FormValue("Y0"), 32)
	srv.imgPage.ImgSetts.XScale, _ = strconv.ParseFloat(r.FormValue("XScale"), 32)
	srv.imgPage.ImgSetts.YScale, _ = strconv.ParseFloat(r.FormValue("YScale"), 32)

	ImgSettsChan <- srv.imgPage.ImgSetts

	http.Redirect(w, r, utils.ParkingWebImagePath, http.StatusFound)
}
