package web

import (
	"html/template"
	"sync"

	"fdps/fdps-parkings/userhub"
	"fdps/utils/logger"
)

type ImgPage struct {
	sync.RWMutex
	editTempl *template.Template
	Title     string

	ImgSetts userhub.ImageSettings
}

func (ip *ImgPage) initialize(title string) {
	ip.Lock()
	defer ip.Unlock()

	var err error
	if ip.editTempl, err = template.New("Img").Parse(ImgTemplate); err != nil {
		logger.PrintfErr("EditImg template Parse ERROR: %v", err)
		return
	}
	ip.Title = title
}

var ImgTemplate = `
<title>{{.Title}}</title>
<h1></h1>

	<form action="/saveImg" method="POST">

	<font size="3" face="verdana" color="black">
	<table width="100%" cellspacing="0" cellpadding="4">
		{{with .ImgSetts}}
			<tr>
				<td colspan="2">Настройки подложки</td>
			</tr>
			<tr>
				<b><td align="left" >Путь к картинке подложки:</td></b>
				<td><input name="ImagePath" type="text" size="200" value={{printf "%s" .ImageFilePath}}></td>
			</tr>
			<tr>
				<td align="left">Географическая долгота левого верхнего угла картинки (градусы):</td>
				<td><input name="X0" type="number" min="-180" max="180" step="any" size="200" value={{printf "%f" .X0}}></td>
			</tr>
			<tr>
				<td align="left">Географическая широта левого верхнего угла картинки (градусы):</td>
				<td><input name="Y0" type="number" min="-90" max="90" step="any" size="200" value={{printf "%f" .Y0}}></td>
			</tr>
			<tr>
				<td align="left">Кол-во градусов долготы в пикселе по оси X:</td>
				<td><input name="XScale" type="number" min="-1" max="1" step="any" size="200" value={{printf "%f" .XScale}}></td>
			</tr>
			<tr>
				<td align="left">Кол-во градусов широты в пикселе по оси Y:</td>
				<td><input name="YScale" type="number" min="-1" max="1" step="any" size="200" value={{printf "%f" .YScale}}></td>
			</tr>
		{{end}}
		<tr> 
    		<td colspan="2"><input type="submit" value="Применить"></td>
		</tr>
	</table>
	</font>
</form>
`
