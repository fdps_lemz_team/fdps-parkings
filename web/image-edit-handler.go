package web

import (
	"net/http"

	"fdps/utils/logger"
)

type ImageEditHandler struct {
	handleURL string
	title     string
}

var ImgEditHdl ImageEditHandler

func (cw ImageEditHandler) Path() string {
	return cw.handleURL
}

func (cw ImageEditHandler) Caption() string {
	return cw.title
}

func (ch ImageEditHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return imgEditHandler
}

func InitImgEditHandler(handleURL string, title string) {
	ImgEditHdl.handleURL = "/" + handleURL
	ImgEditHdl.title = title
}

func imgEditHandler(w http.ResponseWriter, r *http.Request) {
	if err := srv.imgPage.editTempl.ExecuteTemplate(w, "Img", srv.imgPage); err != nil {
		logger.PrintfErr("Img template ExecuteTemplate error: %v", err)
	}
}
