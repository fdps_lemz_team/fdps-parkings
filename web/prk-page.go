package web

import (
	"html/template"
	"sync"

	"fdps/fdps-parkings/userhub"
	"fdps/utils/logger"
)

type PrkPage struct {
	sync.RWMutex
	editTempl *template.Template
	Title     string

	PrkList []userhub.ParkingInfo
}

func (cp *PrkPage) initialize(title string) {
	cp.Lock()
	defer cp.Unlock()

	var err error
	if cp.editTempl, err = template.New("Prk").Parse(PrkTemplate); err != nil {
		logger.PrintfErr("EditPrk template Parse ERROR: %v", err)
		return
	}
	cp.Title = title
}

var PrkTemplate = `
<title>{{.Title}}</title>
<h1></h1>

<form action="/savePrk" method="POST">

<font size="3" face="verdana" color="black">
		<table width="100%" border="1" cellspacing="0" cellpadding="4" >
			<caption style="font-weight:bold">Стоянки</caption>
			<tr>
				<th>Название</th>
				<th>ICAO код аэродрома</th>
				<th>X</th>
				<th>Y</th>
				<th>Правка по X</th>
				<th>Правка по Y</th>
				<th>Объединенная стоянка</th>
			</tr>
			{{with .PrkList}}
				{{range .}}
					<tr align="center">
						<td align="left"> {{.Name}} </td>
						<td align="left"> {{.Airport}} </td>
						<td align="left"> {{.XPos}} </td>
						<td align="left"> {{.YPos}} </td>
						<td align="left"> <input name={{printf "%sXAmmend" .Name}} type="number" min="-100" max="100" size="40" value={{printf "%d" .XAmmend}}> </td>
 						<td align="left"> <input name={{printf "%sYAmmend" .Name}} type="number" min="-100" max="100" size="40" value={{printf "%d" .YAmmend}}> </td>
 						<td align="left"> <input name={{printf "%sUnitedName" .Name}} type="text" size="4" value={{printf "%s" .UnitedName}}> </td>
					</tr>
				{{end}}
			{{end}}
			<tr> 
    			<td colspan="2"><input type="submit" value="Применить"></td>
			</tr>
		</table>
		</font>
</form>
`
