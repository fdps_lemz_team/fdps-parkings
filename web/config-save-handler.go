package web

import (
	"net/http"
	"strconv"
)

type SaveConfigHandler struct {
	handleURL string
	title     string
}

var SaveConfHandler SaveConfigHandler

func (sch SaveConfigHandler) Path() string {
	return sch.handleURL
}

func (sch SaveConfigHandler) Caption() string {
	return sch.title
}

func (sch SaveConfigHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return saveConfigHandler
}

func InitSaveConfigHandler(handleURL string, title string) {
	SaveConfHandler.handleURL = "/" + handleURL
	SaveConfHandler.title = title
}

func saveConfigHandler(w http.ResponseWriter, r *http.Request) {

	aniPort, _ := strconv.Atoi(r.FormValue("AniPort"))

	srv.configPage.AniSetts.Host = r.FormValue("AniHost")
	srv.configPage.AniSetts.Port = aniPort
	srv.configPage.AniSetts.AirdromName = r.FormValue("AniAirdromName")

	AniSettsChan <- srv.configPage.AniSetts

	dbPort, _ := strconv.Atoi(r.FormValue("DbPort"))

	srv.configPage.DbSetts.Hostname = r.FormValue("DbHostname")
	srv.configPage.DbSetts.Port = dbPort
	srv.configPage.DbSetts.ServiceName = r.FormValue("DbServiceName")
	srv.configPage.DbSetts.UserName = r.FormValue("DbUser")
	srv.configPage.DbSetts.Password = r.FormValue("DbPassword")

	DbSettsChan <- srv.configPage.DbSetts

	wsPort, _ := strconv.Atoi(r.FormValue("WsPort"))

	srv.configPage.WsSetts.Port = wsPort

	WsSettsChan <- srv.configPage.WsSetts

	http.Redirect(w, r, "/log", http.StatusFound)
}
