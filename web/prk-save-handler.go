package web

import (
	"fmt"
	"net/http"
	"strconv"

	"fdps/utils"
)

type PrkSaveHandler struct {
	handleURL string
	title     string
}

var PrkSaveHdl PrkSaveHandler

func (sch PrkSaveHandler) Path() string {
	return sch.handleURL
}

func (sch PrkSaveHandler) Caption() string {
	return sch.title
}

func (sch PrkSaveHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return prkSaveHandler
}

func InitPrkSaveHandler(handleURL string, title string) {
	PrkSaveHdl.handleURL = "/" + handleURL
	PrkSaveHdl.title = title
}

func prkSaveHandler(w http.ResponseWriter, r *http.Request) {

	for ind, val := range srv.prkPage.PrkList {
		srv.prkPage.PrkList[ind].XAmmend, _ = strconv.Atoi(r.FormValue(fmt.Sprintf("%sXAmmend", val.Name)))
		srv.prkPage.PrkList[ind].YAmmend, _ = strconv.Atoi(r.FormValue(fmt.Sprintf("%sYAmmend", val.Name)))
		srv.prkPage.PrkList[ind].UnitedName = r.FormValue(fmt.Sprintf("%sUnitedName", val.Name))
	}

	PrkChan <- srv.prkPage.PrkList

	http.Redirect(w, r, utils.ParkingWebParkingsPath, http.StatusFound)
}
