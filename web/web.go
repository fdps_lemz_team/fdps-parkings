// web
package web

import (
	"context"
	"fdps/fdps-parkings/ani"
	"fdps/fdps-parkings/db"
	"fdps/fdps-parkings/userhub"
	"fdps/utils"
	"fmt"
	"log"
	"net/http"
	"time"
)

type httpServer struct {
	http.Server
	shutdownReq chan bool
	done        chan struct{}
	reqCount    uint32
	configPage  *ConfigPage
	prkPage     *PrkPage
	imgPage     *ImgPage
}

var srv httpServer

var AniSettsChan = make(chan ani.Settings, 1)
var DbSettsChan = make(chan db.Settings, 1)
var WsSettsChan = make(chan userhub.Settings, 1)
var PrkChan = make(chan []userhub.ParkingInfo, 1)
var ImgSettsChan = make(chan userhub.ImageSettings, 1)

func Start(done chan struct{}) {
	wsc.load()

	srv = httpServer{
		Server: http.Server{
			Addr:         fmt.Sprintf(":%d", wsc.Port),
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
		//shutdownReq: sdc,
		done:       done,
		configPage: new(ConfigPage),
		prkPage:    new(PrkPage),
		imgPage:    new(ImgPage),
	}
	srv.configPage.initialize("FDPS-PARKING-CONFIG")
	srv.prkPage.initialize("FDPS-PARKING-PARKINGS")
	srv.imgPage.initialize("FDPS-PARKING-IMAGE")

	InitEditConfigHandler(utils.ParkingWebConfigPath, "EDIT PARKING")
	utils.AppendHandler(EditConfHandler)
	InitSaveConfigHandler("saveConfig", "SAVE PARKING")
	utils.AppendHandler(SaveConfHandler)

	InitPrkEditHandler(utils.ParkingWebParkingsPath, "EDIT PARKING")
	utils.AppendHandler(PrkEditHdl)
	InitPrkSaveHandler("savePrk", "SAVE PARKING")
	utils.AppendHandler(PrkSaveHdl)

	InitImgEditHandler(utils.ParkingWebImagePath, "EDIT IMAGE")
	utils.AppendHandler(ImgEditHdl)
	InitImgSaveHandler("saveImg", "SAVE IMAGE")
	utils.AppendHandler(ImgSaveHdl)

	for _, h := range utils.HandlerList {
		http.HandleFunc(h.Path(), h.HttpHandler())
	}

	log.Printf("listening on %d port started", wsc.Port)

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			log.Printf("listen and serve error : %v", err)
		}
	}()
}

func (s *httpServer) stop() {
	log.Printf("stoping http server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	err := s.Shutdown(ctx)

	if err != nil {
		log.Printf("shutdown http server error: %v", err)
	}
}

func SetAniSetts(setts ani.Settings) {
	srv.configPage.AniSetts = setts
}

func SetDbSetts(setts db.Settings) {
	srv.configPage.DbSetts = setts
}

func SetWsSetts(setts userhub.Settings) {
	srv.configPage.WsSetts = setts
}

func SetParkings(prkList []userhub.ParkingInfo) {
	srv.prkPage.PrkList = prkList
}

func SetImgSetts(imgSetts userhub.ImageSettings) {
	srv.imgPage.ImgSetts = imgSetts
}
