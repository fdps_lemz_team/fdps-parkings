package web

import (
	"fmt"
	"net/http"
)

type PrkEditHandler struct {
	handleURL string
	title     string
}

var PrkEditHdl PrkEditHandler

func (cw PrkEditHandler) Path() string {
	return cw.handleURL
}

func (cw PrkEditHandler) Caption() string {
	return cw.title
}

func (ch PrkEditHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return prkEditHandler
}

func InitPrkEditHandler(handleURL string, title string) {
	PrkEditHdl.handleURL = "/" + handleURL
	PrkEditHdl.title = title
}

func prkEditHandler(w http.ResponseWriter, r *http.Request) {
	if err := srv.prkPage.editTempl.ExecuteTemplate(w, "Prk", srv.prkPage); err != nil {
		fmt.Println("template ExecuteTemplate TE ERROR", err)
	}
}
