package web

import (
	"html/template"
	"sync"

	"fdps/fdps-parkings/ani"
	"fdps/fdps-parkings/db"
	"fdps/fdps-parkings/userhub"
	"fdps/utils/logger"
)

type ConfigPage struct {
	sync.RWMutex
	editTempl *template.Template
	Title     string

	AniSetts ani.Settings
	DbSetts  db.Settings
	WsSetts  userhub.Settings
}

func (cp *ConfigPage) initialize(title string) {
	cp.Lock()
	defer cp.Unlock()

	var err error
	if cp.editTempl, err = template.New("Config").Parse(ConfigTemplate); err != nil {
		logger.PrintfErr("EditConfig template Parse ERROR: %v", err)
		return
	}
	cp.Title = title
}

var ConfigTemplate = `
<title>{{.Title}}</title>
<h1></h1>

<form action="/saveConfig" method="POST">

<table width="100%" cellspacing="0" cellpadding="4">
	{{with .AniSetts}}
	<tr>
		<td colspan="2">Настройки подключения к АНИ</td>
	</tr>
	<tr>
		<b><td align="left" width="100">Хост:</td>
		<td><input name="AniHost" type="text" size="40" value={{printf "%s" .Host}}></td>
	</tr>
	<tr>
		<td align="left">Порт:</td>
		<td><input name="AniPort" type="number" min="1024" max="65535" size="40" value={{printf "%d" .Port}}></td>
	</tr>
	<tr>
		<td align="left">Код аэродрома:</td>
		<td><input name="AniAirdromName" type="text" size="4" value={{printf "%s" .AirdromName}}></td>
	</tr>
	{{end}}
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">Настройки подключения к БД</td>
	</tr>
	{{with .DbSetts}}
	<tr>
		<td align="left">Хост:</td>
		<td><input name="DbHostname" type="text" size="40" value={{printf "%s" .Hostname}}></td>
	</tr>
	<tr>
		<td align="left">Порт:</td>
		<td><input name="DbPort" type="number" min="1024" max="65535" size="40" value={{printf "%d" .Port}}></td>
	</tr>
	<tr>
		<td align="left">Имя сервиса:</td>
		<td><input name="DbServiceName" type="text" size="40" value={{printf "%s" .ServiceName}}></td>
	</tr>
	<tr>
		<td align="left">Пользователь:</td>
		<td><input name="DbUser" type="text" size="40" value={{printf "%s" .UserName}}></td>
	</tr>
	<tr>
		<td align="left">Пароль:</td>
		<td><input name="DbPassword" type="text" size="40" value={{printf "%s" .Password}}></td>
	</tr>
	{{end}}
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2">Настройки подключения клиентов</td>
	</tr>
	{{with .WsSetts}}
	<tr>
		<td align="left">Порт:</td>
		<td><input name="WsPort" type="number" min="1024" max="65535" size="40" value={{printf "%d" .Port}}></td>
	</tr>
	{{end}}	
	<tr> 
    	<td colspan="2"><input type="submit" value="Применить"></td>
	</tr>	
</table>
</form>
`
